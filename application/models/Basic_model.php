<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of basic_model
 *
 * @author TOHIR
 */
class Basic_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function fetch_all_records($table, $where = NULL, $rowClass = stdClass::class) {

        if (!$this->db->table_exists($table)) {
            trigger_error("Table `" . $table . "` not exists.", E_USER_ERROR);
        }

        if (is_null($where)) {
            $result = $this->db->get($table)->result($rowClass);
        } else {
            $result = $this->db->get_where($table, $where)->result($rowClass);
        }

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public function saveReg($data) {
        if (empty($data)) {
            return FALSE;
        }
        
        if ($this->_recordExist($data['email'])){
            notify('error', 'Email already exists');
        }

        $this->db->trans_start();
        $this->db->insert('users', $data);
        // send registration email
        $this->_sendConfirmationEmail(array('first_name' => $data['first_name'], 'username' => $data['email']));
        // Send welcome SMS
        SmsAdapter::processViaSMSRoute($data['phone']);
        
        $this->db->trans_complete();

        return TRUE;
    }

   

    private function _sendConfirmationEmail($params) {
        $mail_data = array(
            'header' => 'Welcome to FBN Lounge',
            'first_name' => $params['first_name'],
            'username' => $params['username'],
            'verify_link' => '',
        );

        $msg = $this->load->view('email_templates/reg_confirmation', $mail_data, true);

        return $this->mailer
                ->sendMessage('Welcome | FBN Lounge', $msg, $params['username']);
    }

    private function _recordExist($email) {
        return $this->db->get_where('users', ['email' => $email])->result();
    }

    public function delete($table, $array) {
        $this->db->where($array);
        $q = $this->db->delete($table);
        return $q;
    }
    
    public function deleteBulkMarket($table, $ids) {
        if (empty($ids)){
            return FALSE;
        }
        
        $sql = "DELETE FROM {$table} WHERE market_price_id IN ( ";
        
        $join = '';
        foreach ($ids as $id) {
            $join .= $id . ',' ;
        }
        
        $sql .= rtrim($join, ','). " )";
        
        
        return $this->db->query($sql);
    }

    public function update($table, $data, $where) {
        foreach ($where as $k => $v) {
            $this->db->where($k, $v);
        }
        $query = $this->db->update($table, $data);

        if ($query) {
            return true;
        } else {
            return false;
        }
    }

    public function insert($table, $data) {
        return $this->db->insert($table, $data);
        //return $this->db->insert_id();
    }
    
    public function getMarketById($id) {
        return $this->db->get_where('markets', ['market_id' => $id])->row()->market_name;
    }
    
    public function getProductById($id) {
        return $this->db->get_where('products', ['product_id' => $id])->row()->product_name;
    }

}
