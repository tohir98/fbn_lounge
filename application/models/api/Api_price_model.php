<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Description of Api_price_model
 *
 * @author tohir
 */
class Api_price_model extends CI_Model {

    const TBL_MARKETS = 'markets';
    const TBL_MARKET_PRICES = 'market_prices';
    const TBL_MARKET_W_PRICES = 'market_wholesale_price';
    const TBL_MARKET_R_PRICES = 'market_retail_price';
    const TBL_MARKET_PRODUCTS = 'market_products';

    public function fetchProductPrices($product_id) {
        $markets_products = $this->getMarketProducts($product_id);

        $result = [];

        if (empty($markets_products)) {
            return $result;
        }

        $default = [
            'product_id' => $product_id,
            'product_name' => $markets_products[0]->product_name,
            'product_image' => $markets_products[0]->image_url,
            'description' => $markets_products[0]->product_desc,
            'market_date' => $markets_products[0]->m_date,
        ];

        //var_dump($markets_products); exit;

        foreach ($markets_products as $market) {

            $result['markets'][] = array_merge(['market_name' => $market->market_name], ['wholesale' => $this->_fetchWholePrice($market->mid)], ['retail' => $this->_fetchRetailPrice($market->mid)])
            ;
        }

        return array_merge($default, $result);
    }

    public function getMarketProducts($product_id) {
        return $this->db
                        ->select("mp.* , p.product_name, p.product_desc, m.market_name, pi.image_url, (select market_price_id from market_prices mps where mps.product_id = mp.product_id and mps.market_id=mp.market_id and mps.is_exemption = 0 order by mps.market_price_id desc limit 1 ) as mid, (select market_date from market_prices mps where mps.product_id = mp.product_id and mps.market_id=mp.market_id and mps.is_exemption = 0 order by mps.market_price_id desc limit 1 ) as m_date")
                        ->from('market_products as mp')
                        ->join('products p', 'p.product_id = mp.product_id')
                        ->join('markets m', 'm.market_id = mp.market_id')
                        ->join('product_images pi', 'p.product_id=pi.product_id', 'left')
                        ->where('mp.product_id', $product_id)->get()->result();
    }

    private function _fetchWholePrice($market_price_id) {
        return $this->db
                        ->select('mwp.price_high, mwp.price_low, mwp.price_ave, mw.metric')
                        ->from(self::TBL_MARKET_W_PRICES . ' as mwp')
                        ->join(Setup_model::TBL_M_WHOLESALES . ' as mw', 'mwp.metric_wholesale_id=mw.metric_wholesale_id')
                        ->where('mwp.market_price_id', $market_price_id)
                        ->get()->result();
    }

    private function _fetchRetailPrice($market_price_id) {
        return $this->db
                        ->select('mrp.price_high, mrp.price_low, mrp.price_ave,mr.metric')
                        ->from(self::TBL_MARKET_R_PRICES . ' as mrp')
                        ->join(Setup_model::TBL_M_RETAIL . ' as mr', 'mrp.metric_retail_id=mr.metric_retail_id')
                        ->where('mrp.market_price_id', $market_price_id)
                        ->get()->result();
    }

    private function _getSubCategoriesSerialized($category_id) {
        $result = [];
        $subs = $this->db->get_where('sub_categories', ['category_id' => $category_id])->result();
        if (!empty($subs)) {
            foreach ($subs as $aSub) {
                array_push($result, $aSub->sub_category_id);
            }
        }

        return $result;
    }

    public function getProductsByCategory($category_id, $rowClass = stdClass::class) {
        $subCateIDs = $this->_getSubCategoriesSerialized($category_id);
        if (!empty($subCateIDs)) {
            $this->db->where_in('p.sub_category_id', $subCateIDs);
            $products = $this->db
                        ->select('mp.*, p.product_name, p.season_id, m.market_name, pi.image_url, (SELECT count(1) FROM market_products mp WHERE p.product_id = mp.product_id) as kount')
                        ->from(Setup_model::TBL_PRODUCTS . ' as p')
                        ->join(self::TBL_MARKET_PRODUCTS . ' as mp', 'mp.product_id=p.product_id')
                        ->join('product_images as pi', 'p.product_id=pi.product_id', 'left')
                        ->join(Setup_model::TBL_MARKETS . ' as m', 'mp.market_id=m.market_id')
                        ->group_by('p.product_id')
                        ->get()->result($rowClass);
        }else{
            $products = [];
        }
        return $products;
    }

}
