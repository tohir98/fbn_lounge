<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Description of User_controller
 *
 * @author TOHIR
 * 
 * @property CI_Loader $load Description
 * @property User_model $u_model Description
 * @property Basic_model $basic_model Description
 * @property SmsAdapter $SmsAdapter Description
 */
class User_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->helper(['auth_helper', 'notification_helper']);
        $this->load->library(['mailer', 'SmsAdapter']);

        $this->load->model('basic_model');
    }

    public function register() {
        if (request_is_post()) {
            if ($this->basic_model->saveReg(request_post_data())) {
                notify('success', "Registration was successful");
            } else {
                notify('error', "Ops! A problem occured while completing your registration. Pls try again");
            }
            redirect(site_url('/reg_success'));
        }
        $data = [
            'states' => $this->basic_model->fetch_all_records('states'),
            'job_categories' => $this->basic_model->fetch_all_records('job_categories'),
            'title' => "FBN Lounge Registration"
        ];
        $this->load->view('templates/register', $data);
    }

    public function reg_success() {
        $this->load->view('templates/success');
    }
    
    public function reg_error() {
        $this->load->view('templates/error');
    }
    
    public function questionaire() {
        $data = [
            'job_categories' => $this->basic_model->fetch_all_records('job_categories'),
            'earnings' => $this->basic_model->fetch_all_records('salary_range'),
            'tripLengths' => $this->basic_model->fetch_all_records('trip_length'),
            'tripCosts' => $this->basic_model->fetch_all_records('average_trip_cost'),
            'title' => "FBN Lounge Traveler's Questionnaire"
            ];
        $this->load->view('templates/questionaire', $data);
    }

    public function logout() {
        $this->mixpanel_lib->track(['message' => "User logout successfully", 'action' => 'Logout']);
        $this->user_auth_lib->logout();
        redirect(site_url('/login'));
    }

}
