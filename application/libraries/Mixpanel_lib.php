<?php

/**
 * Description of Mixpanel_lib
 *
 * @author tohir
 */
class Mixpanel_lib {

    private $mp;

    public function __construct() {
        $this->CI = & get_instance();
        
        $this->_init();
    }

    private function _init() {
        if (!$this->mp) {
            $this->mp = Mixpanel::getInstance(MIXPANEL_PROJECT_TOKEN);
        }
    }
    
    /**
     * 
     * $params['message'] Message
     * $params['action'] Action name
     * @param type $params
     */
    public function track($params) {
        $this->mp->track($params['message'], array("label" => $params['action']));
    }

}
