<?php

use sms\Sms_lib;

/**
 * Description of SmsAdapter
 *
 * @author Tohir
 */
class SmsAdapter {

    //put your code here
    protected static $message = "Welcome to Firstbank Lounge, explore our products & services while you await your flight. We wish you a safe trip. FirstBank.. You First";

    public static function processViaSMSRoute($mobiles) {
        if ($mobiles != '') {

            return Sms_lib::sendSms(static::preparePhoneNumber($mobiles), static::$message);
        }
    }

    private static function preparePhoneNumber($phone) {
        if (substr($phone, 0, 1) === '0') {
            $phone = '234' . substr($phone, 1);
        }

        return $phone;
    }

}

//var_dump(SmsAdapter::processViaSMSRoute('2348023611841', 'Test message'));
