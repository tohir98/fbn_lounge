<?php

namespace api;

/**
 * Description of Products
 *
 * @author tohir
 */
class Products extends \db\Entity {
    public function jsonSerialize() {
        return array(
            'product_id' => $this->product_id,
            'product_name' => $this->product_name,
//            'market_id' => $this->market_id,
//            'market' => $this->market_name,
            'season' => $this->season_id,
            'image' => $this->image_url,
            'available' => $this->kount
        );
    }
}
