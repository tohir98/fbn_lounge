<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace utils;

/**
 * Description of Enums
 *
 * @author JosephT
 */
class Enums {

    //put your code here

    public static function getApprovalStatuses() {
        return [
            //APPROVAL_STATUS_PENDING => 'Pending',
            APPROVAL_STATUS_PENDING => 'Awaiting Submission',
            APPROVAL_STATUS_APPROVED => 'Approved',
            APPROVAL_STATUS_CANCELED => 'Cancelled',
            APPROVAL_STATUS_DECLINED => 'Declined',
            APPROVAL_STATUS_SUBMITTED => 'Submitted',
        ];
    }

    public static function getTaskStatuses() {
        return array(
            'pending' => TASK_STATUS_TO_DO,
            'completed' => TASK_STATUS_COMPLETE,
            'in_progress' => TASK_STATUS_IN_PROGRESS,
            'declined' => TASK_STATUS_DECLINED,
            'canceled' => TASK_STATUS_CANCELED,
        );
    }

    public static function getUsersAccountStatuses() {
        return array(
            USER_STATUS_INACTIVE => 'Inactive',
            USER_STATUS_ACTIVE => 'Active',
            USER_STATUS_SUSPENDED => 'Suspended',
            USER_STATUS_TERMINATED => 'Terminated',
            USER_STATUS_PROBATION_ACCESS => 'Probation_access',
            USER_STATUS_ACTIVE_NO_ACCESS => 'Active_no_access',
            USER_STATUS_PROBATION_NO_ACCESS => 'Probation_no_access'
        );
    }

    public static function getChecklistCompletionStatuses() {
        return array(
            CHECKLIST_STATUS_COMPLETED => 'Completed',
            CHECKLIST_STATUS_ONGOING => 'Ongoing',
            CHECKLIST_STATUS_PENDING => 'Pending',
            CHECKLIST_STATUS_CANCELED => 'Cancelled',
        );
    }

    public static function getUserActionStatuses() {
        return array(
            LOG_TYPE_LOGIN => 'LOGGED IN',
            LOG_TYPE_LOGOUT => 'LOGGED OUT',
            LOG_TYPE_BLOCKED => 'ACCOUNT BLOCKED'
        );
    }
	
	public static function getTBAdminActionStatuses() {
		return array(
			LOG_TYPE_TB_LOGIN => 'LOGGED IN',
			LOG_TYPE_TB_LOGOUT => 'LOGGED OUT',
			LOG_TYPE_TB_PROXY_ACCESS => 'PROXY ACCESS'
		);
	}
	
	public static function getAppraisalPeriodStatus() {
		return array(
			FREQUENCY_WEEKLY => 'Weekly',
			FREQUENCY_MONTHLY => 'Monthly',
			FREQUENCY_QUARTERLY => 'Quaterly',
			FREQUENCY_SEMI_ANNUAL => 'Semi-annual',
			FREQUENCY_ANNUAL => 'Annual'
		);
	}

	public static function getWorkExperience() {
		return array(
			1 => '0-1',
			2 => '1-2',
			3 => '2-4',
			4 => '4-6',
			5 => '6-8',
			6 => '8-10',
			7 => '10-12',
			8 => '12-14',
			9 => '14+'
		);
	}
	
	public static function getJobStatus() {
		return array (
			RECRUITING_STATUS_CANCELLED => 'Cancelled',
			RECRUITING_STATUS_PUBLISHED => 'Published',
			RECRUITING_STATUS_PENDING => 'Pending',
			RECRUITING_STATUS_DRAFT => 'Draft',
			RECRUITING_STATUS_CLOSED => 'Closed'
		);
	}
	
	public static function getAppraisalRoles() {
		return array(
			1 => 'Administrator',
			2 => 'Unit Manager',
			3 => 'Employee',
			4 => 'Appraisee'
		);
	}
	
	public static function  getEventTypes() {
		return array(
			'Task' => 1,
			'Birthday' => 2,
			'Work_Anniversary' => 3,
			'Public_Holiday' => 4,
			'Approved_Leave' => 5,
			'Training' => 6,
			'Events' => 7
		);
	}
}
