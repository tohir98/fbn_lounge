<?php

/**
 * Description of Lib
 *
 * @author tohir
 */
abstract class Lib {
    
     public function __construct() {
        $this->load->database();
    }
    
    public function __get($name) {
        return get_instance()->$name;
    }
}
