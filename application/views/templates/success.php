<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html  lang="en">
    <?php include "header.php"; ?>


    <body class="closed-sidebar">
        <div id='sb-site'>

            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id='page-wrapper'>
                <div id="page-header" class="bg-gradient-9">
                    <div id="mobile-navigation">
                        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
                        <a href="#" class="logo-content-small" title="FBN Lounge"></a>
                    </div>

                    <div id="header-nav-left">
                        <div class="user-account-btn dropdown">
                            <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                                <img width="180" src="assets/images/fbn_logo.png" alt="FBN Lounge">
                                <span>FBN Lounge</span>
                            </a>
                        </div>
                    </div><!-- #header-nav-left -->

                    <div id="header-nav-right">
                        <a href="#" class="hdr-btn" id="fullscreen-btn" title="Fullscreen">
                            <i class="glyph-icon icon-arrows-alt"></i>
                        </a>

                    </div><!-- #header-nav-right -->

                </div>
                
                <div id="page-content-wrapper">
                    <div id="page-content">
                        <h3>Registration completed successfully</h3>
                    </div>
                </div>
            </div>
            
            <?php include "footer.php"; ?>
        </div>
    </body>
</html>