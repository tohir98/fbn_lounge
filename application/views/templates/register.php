<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html  lang="en">
    <?php include "header.php"; ?>


    <body class="closed-sidebar">
        <div id="sb-site">
            
            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <?php include 'top_menu.php'; ?>
                <div id="page-content-wrapper">
                    <div id="page-content">




                        <!-- Bootstrap Wizard -->

                        <!--<link rel="stylesheet" type="text/css" href="assets/widgets/wizard/wizard.css">-->
                        <script type="text/javascript" src="assets/widgets/wizard/wizard.js"></script>
                        <script type="text/javascript" src="assets/widgets/wizard/wizard-demo.js"></script>

                        <!-- Boostrap Tabs -->

                        <script type="text/javascript" src="assets/widgets/tabs/tabs.js"></script>

                        <div id="page-title">
                            <h2>User Registration Form</h2>
                            <p>For first time visitors.</p>

                        </div>
                        
                        <?= show_notification(); ?>

                        <div class="panel">
                            <div class="panel-body">
                                <!-- <h3 class="title-hero">
                                    Alternate style
                                </h3> -->
                                <div class="example-box-wrapper">
                                    <div id="form-wizard-3" class="form-wizard">
                                        <ul>
                                            <li>
                                                <a href="#step-1" data-toggle="tab" id="link1">
                                                    <label class="wizard-step">1</label>
                                                    <span class="wizard-description">
                                                        User details
                                                        <small>Gather the user details</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-2" data-toggle="tab" id="link2">
                                                    <label class="wizard-step">2</label>
                                                    <span class="wizard-description">
                                                        Contact information
                                                        <small>Confirm contact details</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-3" data-toggle="tab" id="link3">
                                                    <label class="wizard-step">3</label>
                                                    <span class="wizard-description">
                                                        Work Information
                                                        <small>Details about your occupation</small>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#step-4" data-toggle="tab" id="link4">
                                                    <label class="wizard-step">4</label>
                                                    <span class="wizard-description">
                                                        Final steps
                                                        <small>Finish and send the email</small>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <form class="form-horizontal bordered-row" id="demo-form" method="post">
                                                <div class="tab-pane active" id="step-1">
                                                    <?php include "forms/_step1.php"; ?>
                                                    <div class="pull-right">
                                                        <a href="#step-2" data-toggle="tab" class="btn btn-default" id="btn1"> Next
                                                            <i class="glyph-icon icon-arrow-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="step-2">
                                                    <?php include "forms/_step2.php"; ?>
                                                    <div class="pull-left">
                                                        <a href="#step-1" data-toggle="tab" class="btn btn-default" id="back_btn1"> <i class="glyph-icon icon-arrow-left"></i> Previous
                                                        </a>
                                                    </div>
                                                    <div class="pull-right">
                                                        <a href="#step-3" data-toggle="tab" class="btn btn-default button-next" id="btn2"> Next
                                                            <i class="glyph-icon icon-arrow-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="step-3">
                                                    <?php include "forms/_step3.php"; ?>
                                                    <div class="pull-left">
                                                        <a href="#step-2" data-toggle="tab" class="btn btn-default" id="back_btn2"> <i class="glyph-icon icon-arrow-left"></i> Previous
                                                        </a>
                                                    </div>
                                                    <div class="pull-right">
                                                        <a href="#step-4" data-toggle="tab" class="btn btn-default button-next" id="btn3"> Next
                                                            <i class="glyph-icon icon-arrow-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="step-4">
                                                    <div class="content-box">
                                                        <h3 class="content-box-header" style="background-color: #09486D; color: #fff">
                                                            Do you have any account with First Bank?
                                                        </h3>
                                                        <div class="content-box-wrapper">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label">First Bank Account</label>
                                                                <div class="col-sm-6">
                                                                    <label class="radio-inline">
                                                                        <input type="radio" id="" name="fbn_account" value="1">
                                                                        Yes
                                                                    </label>
                                                                    <label class="radio-inline">
                                                                        <input type="radio" id="" name="fbn_account" value="0">
                                                                        No
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pull-left">
                                                        <a href="#step-3" data-toggle="tab" class="btn btn-default" id="back_btn3"> 
                                                            <i class="glyph-icon icon-arrow-left"></i> Previous
                                                        </a>
                                                    </div>
                                                    <div class="pull-right">
                                                        <button class="btn btn-primary" type="submit"> Save Registration</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

            <?php include "footer.php"; ?>

        </div>
        
        <script>
            $("#btn1").click(function(){
                $("#link2").click();
            });
            $("#btn2").click(function(){
                $("#link3").click();
            });
            $("#btn3").click(function(){
                $("#link4").click();
            });
            $("#back_btn1").click(function(){
                $("#link1").click();
            });
            $("#back_btn2").click(function(){
                $("#link2").click();
            });
            $("#back_btn3").click(function(){
                $("#link3").click();
            });
        </script>
    </body>
</html>
