<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html  lang="en">
    <?php include "header.php"; ?>


    <body class="closed-sidebar">
        <div id="sb-site">

            <div id="loading">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

            <div id="page-wrapper">
                <?php include 'top_menu.php'; ?>
                <div id="page-content-wrapper">
                    <div id="page-content">




                        <!-- Bootstrap Wizard -->

                        <!--<link rel="stylesheet" type="text/css" href="assets/widgets/wizard/wizard.css">-->
                        <script type="text/javascript" src="assets/widgets/wizard/wizard.js"></script>
                        <script type="text/javascript" src="assets/widgets/wizard/wizard-demo.js"></script>

                        <!-- Boostrap Tabs -->

                        <script type="text/javascript" src="assets/widgets/tabs/tabs.js"></script>

                        <div id="page-title">
                            <h2>Traveler's Questionnaire</h2>

                        </div>

                        <?= show_notification(); ?>

                        <div class="panel">
                            <div class="panel-body">
                                <!-- <h3 class="title-hero">
                                    Alternate style
                                </h3> -->
                                <div class="example-box-wrapper">
                                    <div id="form-wizard-3" class="form-wizard">

                                        <div class="tab-content">
                                            <form class="form-horizontal bordered-row" id="demo-form" method="post">
                                                <div class="tab-pane active" id="step-1">
                                                    <?php include "questionaire/question_page.php"; ?>
                                                    <div class="pull-right">
                                                        <a href="#step-2" data-toggle="tab" class="btn btn-default" id="btn1"> Next
                                                            <i class="glyph-icon icon-arrow-right"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="tab-pane" id="step-2">
                                                    <?php include "questionaire/question_page2.php"; ?>
                                                    <div class="pull-left">
                                                        <a href="#step-1" data-toggle="tab" class="btn btn-default" id="back_btn1"> <i class="glyph-icon icon-arrow-left"></i> Previous
                                                        </a>
                                                    </div>
                                                    <div class="pull-right">
                                                        <a href="#step-3" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i></a>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="step-3">
                                                    <div class="content-box">
                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">Where do you like to stay?</label>
                                                                        <div class="col-sm-6">
                                                                            <select required="" class="form-control" id="travel_companion" name="travel_companion">
                                                                                <option value="">Select</option>
                                                                                <option value="1">Expensive Hotels</option>
                                                                                <option value="2">Economy Hotels</option>
                                                                                <option value="3">Resorts</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pull-left">
                                                        <a href="#step-2" data-toggle="tab" class="btn btn-default"> <i class="glyph-icon icon-arrow-left"></i> Previous
                                                        </a>
                                                    </div>
                                                    <div class="pull-right">
                                                        <a href="#step-4" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                        </a>
                                                    </div>

                                                </div>

                                                <div class="tab-pane" id="step-4">
                                                    <div class="content-box">
                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">How do you plan your trip:</label>
                                                                        <div class="col-sm-6">
                                                                            <select required="" class="form-control" id="trip_planner" name="trip_planner">
                                                                                <option value="">Select</option>
                                                                                <option value="1">By Yourself</option>
                                                                                <option value="2">Through a Travel Agent</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="pull-left">
                                                        <a href="#step-3" data-toggle="tab" class="btn btn-default"> <i class="glyph-icon icon-arrow-left"></i> Previous
                                                        </a>
                                                    </div>
                                                    <div class="pull-right">
                                                        <a href="#step-5" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                        </a>
                                                    </div>

                                                </div>

                                                <div class="tab-pane" id="step-5">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label">What is the most common type purpose for your travel?</label>
                                                                        <div class="col-sm-5">
                                                                            <select required="" class="form-control" id="travel_purpose" name="travel_purpose">
                                                                                <option value="">Select</option>
                                                                                <option value="1">Family Vacation</option>
                                                                                <option value="2">Adventures</option>
                                                                                <option value="3">Business</option>
                                                                                <option value="4">Spiritual</option>
                                                                                <option value="5">Religious</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-4" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-6" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="step-6">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">How often do you travel:</label>
                                                                        <div class="col-sm-6">
                                                                            <select required="" class="form-control" id="travel_frequency" name="travel_frequency">
                                                                                <option value="">Select</option>
                                                                                <option value="1">Once in a Year</option>
                                                                                <option value="2">Once in a 6 Months</option>
                                                                                <option value="3">Once in a 3 Months</option>
                                                                                <option value="4">Once in a Months</option>
                                                                                <option value="5">Every Months</option>
                                                                                <option value="6">Every Week</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-5" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-7" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="step-7">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">Where do you go most often?</label>
                                                                        <div class="col-sm-6">
                                                                            <select required="" class="form-control" id="travel_purpose" name="travel_purpose">
                                                                                <option value="">Select</option>
                                                                                <option value="1">Hill Station</option>
                                                                                <option value="2">Beaches</option>
                                                                                <option value="3">Deserts</option>
                                                                                <option value="4">Historically Significant Places</option>
                                                                                <option value="5">Commercially busy cities</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-6" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-8" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="step-8">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-4 control-label">How many times have you traveled Internationally?</label>
                                                                        <div class="col-sm-5">
                                                                            <input type="number" name="trip_count" id="trip_count" class="form-control" min="0" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-7" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-9" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="step-9">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">With whom do you travel most often?</label>
                                                                        <div class="col-sm-6">
                                                                            <select required="" class="form-control" id="travel_companion" name="travel_companion">
                                                                                <option value="">Select</option>
                                                                                <option value="1">Family</option>
                                                                                <option value="2">Friends</option>
                                                                                <option value="3">Colleagues</option>
                                                                                <option value="4">Alone</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-8" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-10" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="tab-pane" id="step-10">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">Income Level (Per annum):</label>
                                                                        <div class="col-sm-6">
                                                                            <select name="job_category_id" id="job_category_id" class="form-control">
                                                                                <option value="" selected="selected">Select Income Level</option>
                                                                                <?php
                                                                                if (!empty($earnings)):
                                                                                    foreach ($earnings as $earning):
                                                                                        ?>
                                                                                        <option value="<?= $earning->id_salary; ?>"><?= $earning->name; ?></option>
                                                                                        <?php
                                                                                    endforeach;
                                                                                endif;
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-9" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-11" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane" id="step-11">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">What is the general length of the trip:</label>
                                                                        <div class="col-sm-6">
                                                                            <select name="trip_length" id="trip_length" class="form-control">
                                                                                <option value="" selected="selected">Select Trip Length</option>
                                                                                <?php
                                                                                if (!empty($tripLengths)):
                                                                                    foreach ($tripLengths as $tripLen):
                                                                                        ?>
                                                                                        <option value="<?= $tripLen->trip_length_id; ?>"><?= $tripLen->length; ?></option>
                                                                                        <?php
                                                                                    endforeach;
                                                                                endif;
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-10" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-12" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane" id="step-12">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">Do you have membership card of any holiday of club?:</label>
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" name="membership_card" id="membership_card_yes" value="yes" /> Yes
                                                                            <input type="radio" name="membership_card" id="membership_card_no" value="No" /> No
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-11" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-13" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane" id="step-12">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">Do you have any favorite travel website?:</label>
                                                                        <div class="col-sm-6">
                                                                            <input type="text" name="fav_travel_site" id="fav_travel_site"  />
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-11" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-13" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="tab-pane" id="step-13">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">How much do you spend on a trip average?:</label>
                                                                        <div class="col-sm-6">
                                                                            <select name="average_trip_cost" id="average_trip_cost" class="form-control">
                                                                                <option value="" selected="selected">Select Average Cost</option>
                                                                                <?php
                                                                                if (!empty($tripCosts)):
                                                                                    foreach ($tripCosts as $tripCost):
                                                                                        ?>
                                                                                        <option value="<?= $tripCost->average_trip_cost_id; ?>"><?= $tripCost->cost; ?></option>
                                                                                        <?php
                                                                                    endforeach;
                                                                                endif;
                                                                                ?>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-12" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <a href="#step-14" data-toggle="tab" class="btn btn-default" > Next <i class="glyph-icon icon-arrow-right"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                 <div class="tab-pane" id="step-14">
                                                    <div class="content-box">

                                                        <div class="content-box-wrapper">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="form-group">
                                                                        <label class="col-sm-3 control-label">Do you wish to go for a world tour?:</label>
                                                                        <div class="col-sm-6">
                                                                            <input type="radio" name="world_tour" id="world_tour_yes" value="yes" /> Yes
                                                                            <input type="radio" name="world_tour" id="world_tour_no" value="No" /> No
                                                                        </div>
                                                                    </div>
                                                                    <div class="pull-left">
                                                                        <a href="#step-13" data-toggle="tab" class="btn btn-default" > <i class="glyph-icon icon-arrow-left"></i> Previous
                                                                        </a>
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <button type="submit" class="btn btn-success">Submit</button>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>
            </div>

            <?php include "footer.php"; ?>

        </div>

        <script>
            $("#btn1").click(function () {
                $("#link2").click();
            });
            $("#btn2").click(function () {
                $("#link3").click();
            });
            $("#btn3").click(function () {
                $("#link4").click();
            });
            $("#back_btn1").click(function () {
                $("#link1").click();
            });
            $("#back_btn2").click(function () {
                $("#link2").click();
            });
            $("#back_btn3").click(function () {
                $("#link3").click();
            });
        </script>
    </body>
</html>
