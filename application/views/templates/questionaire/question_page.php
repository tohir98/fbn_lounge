<div class="content-box">
    <!-- <h3 class="content-box-header bg-default">
        Biodata
    </h3> -->
    <div class="content-box-wrapper">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label class="col-sm-3 control-label">First Name:</label>
                      <div class="col-sm-6">
                          <input type="text" placeholder="First name" required="" class="form-control" name="first_name" id="first_name" >
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Last Name:</label>
                      <div class="col-sm-6">
                          <input type="text" placeholder="Last name" required="" class="form-control" name="last_name" id="last_name" >
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Age:</label>
                      <div class="col-sm-6">
                          <input type="number" required="" class="form-control" name="age" id="age" >
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Gender:</label>
                      <div class="col-sm-6">
                          <select name="gender_id" id="gender_id" class="form-control">
                            <option value="" selected="selected">Select Gender</option>
                            <option value="1">Female</option>
                            <option value="2">Male</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Residential Address:</label>
                      <div class="col-sm-6">
                          <input type="text" placeholder="address" required="" class="form-control" name="address" id="address">
                      </div>
                  </div>
                  
              </div>

          </div>
    </div>
</div>
