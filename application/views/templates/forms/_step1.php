<div class="content-box">
    <!-- <h3 class="content-box-header bg-default">
        Biodata
    </h3> -->
    <div class="content-box-wrapper">
          <div class="row">
              <div class="col-md-12">
                  <div class="form-group">
                      <label class="col-sm-3 control-label">First Name:</label>
                      <div class="col-sm-6">
                          <input type="text" placeholder="First name" required="" class="form-control" name="first_name" id="first_name" >
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Last Name:</label>
                      <div class="col-sm-6">
                          <input type="text" placeholder="Last name" required="" class="form-control" name="last_name" id="last_name" >
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Email:</label>
                      <div class="col-sm-6">
                          <input type="email" placeholder="Email" required="" class="form-control" name="email" id="email" >
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Phone:</label>
                      <div class="col-sm-6">
                          <input type="text" placeholder="Phone" required="" class="form-control" name="phone" id="phone" maxlength="11" >
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Gender:</label>
                      <div class="col-sm-6">
                          <select name="gender_id" id="gender_id" class="form-control">
                            <option value="" selected="selected">Select Gender</option>
                            <option value="1">Female</option>
                            <option value="2">Male</option>
                          </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <label class="col-sm-3 control-label">Marital Status:</label>
                      <div class="col-sm-6">
                          <select name="marital_status_id" id="marital_status_id" class="form-control">
                            <option value="" selected="selected">Select Marital Status</option>
                            <option value="1">Divorced</option>
                            <option value="2">Married</option>
                            <option value="3">Separated</option>
                            <option value="4">Single</option>
                          </select>
                      </div>
                  </div>


              </div>

          </div>
    </div>
</div>
