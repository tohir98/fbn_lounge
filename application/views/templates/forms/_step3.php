<div class="content-box">
    <!-- <h3 class="content-box-header bg-default">
        Biodata
    </h3> -->
    <div class="content-box-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Category:</label>
                    <div class="col-sm-6">
                        <select name="job_category_id" id="job_category_id" class="form-control">
                            <option value="" selected="selected">Select Job Category</option>
                            <?php
                            if (!empty($job_categories)):
                                foreach ($job_categories as $jc):
                                    ?>
                                    <option value="<?= $jc->id_category; ?>"><?= $jc->title; ?></option>
                                <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Name:</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Company Name" required="" class="form-control" name="company_name" id="company_name" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Company Location:</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Location" required="" class="form-control" name="company_location" id="company_location" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Designation:</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Designation" required="" class="form-control" name="designation" id="designation" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Job Title:</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Job Title" required="" class="form-control" name="job_title" id="job_title" >
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
