<div class="content-box">
    <!-- <h3 class="content-box-header bg-default">
        Biodata
    </h3> -->
    <div class="content-box-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Address:</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="Address" class="form-control" name="address" id="address" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">City:</label>
                    <div class="col-sm-6">
                        <input type="text" placeholder="City" class="form-control" name="city" id="city" >
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">State</label>
                    <div class="col-sm-6">
                        <select required="" class="form-control" id="state_id" name="state_id">
                            <option value="">Select State</option>
                             <?php
                            if (!empty($states)):
                                foreach ($states as $state):
                                    ?>
                                    <option value="<?= $state->id_state; ?>"><?= $state->state; ?></option>
                                <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
