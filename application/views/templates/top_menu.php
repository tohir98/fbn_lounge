<div id="page-header" style="background-color: #09486D">
    <div id="mobile-navigation">
        <button id="nav-toggle" class="collapsed" data-toggle="collapse" data-target="#page-sidebar"><span></span></button>
        <a href="#" class="logo-content-small" title="FBN Lounge"></a>
    </div>

    <div id="header-nav-left">
        <div class="user-account-btn dropdown">
            <a href="#" title="My Account" class="user-profile clearfix" data-toggle="dropdown">
                <img width="180" src="assets/images/fbn_logo.png" alt="FBN Lounge">
                <span>FBN Lounge</span>
            </a>
        </div>
    </div><!-- #header-nav-left -->

    <div id="header-nav-right">
        <a href="#" class="hdr-btn" id="fullscreen-btn" title="Fullscreen">
            <i class="glyph-icon icon-arrows-alt"></i>
        </a>

    </div><!-- #header-nav-right -->

</div>