<?php include '_header.php'; ?>
<p><strong>Hi <?php echo ucfirst($first_name); ?>,</strong></p>
<p>
    Welcome to Firstbank Lounge, explore our products and services while you await your flight. We wish you a safe trip.
</p>
<p>FirstBank... You First</p>

Thanks<br/><br/>

Regards,<br/>
<?= BUSINESS_NAME; ?>

<br/>
<br/>

<?php include '_footer.php'; ?>