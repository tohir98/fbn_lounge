<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Navigation helper
 */
if (!function_exists('build_top_nav_part')) {

    function build_top_nav_part($nav_arr, $main_link = NULL) {
        // $CI =& get_instance();

        $combined_setup = array(
            'vacation',
            'compensation',
            'employee_request',
            'loans',
            'attendance',
            'payroll',
            'training',
            'appraisal',
            'goals',
        );

        $single_setup = array();

        $no_setup = array(
            'access_control',
            'account_settings',
            'employee_purchases',
            'hr_clients',
            'employee',
            'organisations',
            'billings',
            'rewards',
            'recruiting',
            'suggestions',
            'personal',
            'events',
            'document_center',
            'employee_queries'
        );

        // Load CI object & model
        $CI = &get_instance();
        $CI->load->model('vacation/vacation_model', 'v_model');

        $buffer = '<li>
				       <a href="' . site_url($main_link . '/dashboard') . '">Dashboard</a>
				   </li>';

        if (!is_array($nav_arr) or empty($nav_arr)) {
            return $buffer;
        }

        foreach ($nav_arr as $label => $modules) {

            $buffer .= '<li>
					<a href="#" data-toggle="dropdown" class="dropdown-toggle">
						<span>' . $label . '</span>
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu">';

            foreach ($modules as $id => $item) {
                $setup = 0;

                //edited by obinna ukpabi
                $isLoggedIn = $CI->session->userdata('id_user');
                $id_company = $CI->session->userdata('id_company');
                $employee = $CI->v_model->get('employees', array('id_user' => $isLoggedIn));

                if (in_array($item['id_string'], $no_setup) || $employee[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR || $employee[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
                    $setup = 1;
                } elseif (in_array($item['id_string'], $combined_setup) || $employee[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR || $employee[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
                    $md = $CI->v_model->get('modules', array('id_string' => $item['id_string']));
                    $module_setup = $CI->v_model->get('module_setup', array('id_module' => $md[0]->id_module, 'status' => MODULE_SETUP_ACTIVE, 'id_company' => $id_company));

                    if (!empty($module_setup)) {
                        $setup = 1;
                    }
                } else {
                    $setup = 0;
                }

                if ($setup > 0) {
                    if (count($modules) == 1) {

                        foreach ($item['items'] as $a => $c) {
                            if ($c['in_menu'] == 1) { 
                                $buffer .= '<li><a href="' . site_url($c['module_id_string'] . '/' . $c['perm_id_string']) . '">' . $c['perm_subject'] . '</a></li>';
                            }
                        }
                    } else {

                        $buffer .= '<li class="dropdown-submenu">
						<a href="#" data-toggle="dropdown" class="dropdown-toggle">
							<span>' . $item['subject'] . '</span>
						</a>
						<ul class="dropdown-menu">';

                        foreach ($item['items'] as $i => $v) {

                            if ($v['in_menu'] == 1) { 
                                $buffer .= '<li><a href="' . site_url($v['module_id_string'] . '/' . $v['perm_id_string']) . '">' . $v['perm_subject'] . '</a></li>';
                            }
                        }



                        unset($setup);

                        $buffer .= '</ul>';
                        $buffer .= '</li>';
                    }
                }
            } // End modules

            $buffer .= '</ul>';
            $buffer .= '</li>';
        } // End nav_arr

        return $buffer;
    }

}

if (!function_exists('build_sidebar_nav_part')) {

    function build_sidebar_nav_part($nav_arr) {

        if (!is_array($nav_arr) or empty($nav_arr)) {
            return false;
        }

        // Load CI object
        $CI = get_instance();

        // Load libraries
        $CI->load->library('uri');

        $segment1 = $CI->uri->segment(1);
        $segment2 = $CI->uri->segment(2);

        if (($segment1 == 'admin' or $segment1 == 'employee') and $segment2 == 'dashboard') {
            return build_sidebar_nav_part_all($nav_arr);
        } else {
            return build_sidebar_nav_part_module($nav_arr, $segment1);
        }
    }

}

if (!function_exists('build_sidebar_nav_part_module')) {

    function build_sidebar_nav_part_module($nav_arr, $module) {

        $buffer = '';

        foreach ($nav_arr as $subject => $modules) {

            foreach ($modules as $item => $value) {

                if ($item == $module) {

                    $buffer .= '<div class="subnav">
				<div class="subnav-title">
					<a href="#" class="toggle-subnav"><i class="icon-angle-down"></i><span>' . $value['subject'] . '</span></a>
				</div>
				<ul class="subnav-menu">';

                    foreach ($value['items'] as $i) {

                        if ($i['in_menu'] == 1) {
                            $buffer .= '<li>
									<a href="' . site_url($i['module_id_string'] . '/' . $i['perm_id_string']) . '">' . $i['perm_subject'] . '</a>
								</li>';
                        }
                    }

                    $buffer .= '</ul></div>';
                } // End if item
            }
        } // End groups

        return $buffer;
    }

// End func build_sidebar_nav_part_all
}

if (!function_exists('build_sidebar_nav_part_all')) {

    function build_sidebar_nav_part_all($nav_arr) {
        return '';
        $buffer = '';

        foreach ($nav_arr as $subject => $modules) {

            $buffer .= '<div class="subnav">
				<div class="subnav-title">
					<a href="#" class="toggle-subnav"><i class="icon-angle-down"></i><span>' . $subject . '</span></a>
				</div>
				<ul class="subnav-menu">';

            foreach ($modules as $item => $value) {

                if (count($modules) == 1) {

                    foreach ($value['items'] as $i) {

                        if ($i['in_menu'] == 1) {
                            $buffer .= '<li>
										<a href="' . site_url($i['module_id_string'] . '/' . $i['perm_id_string']) . '">' . $i['perm_subject'] . '</a>
									</li>';
                        }
                    }
                } else {

                    $buffer .= '<li class="dropdown">
									<a href="#" data-toggle="dropdown">' . $value['subject'] . '</a>
									<ul class="dropdown-menu">';

                    foreach ($value['items'] as $p) {
                        if ($p['in_menu'] == 1) {
                            $buffer .= '<li>
										<a href="' . site_url($p['module_id_string'] . '/' . $p['perm_id_string']) . '">' . $p['perm_subject'] . '</a>
									</li>';
                        }
                    }

                    $buffer .= '</ul></li>';
                }
            }

            $buffer .= '</ul></div>';
        } // End groups

        return $buffer;
    }

// End func build_sidebar_nav_part_all
}

if (!function_exists('build_dashboard_buttons_nav_part')) {

    function build_dashboard_buttons_nav_part($nav_arr) {

        $base_arr = array(
            'employee' => array(
                'icon' => 'icon-user',
                'color' => 'blue'
            ),
            'vacation' => array(
                'icon' => 'icon-plane',
                'color' => 'satblue'
            ),
            'appraisal' => array(
                'icon' => 'icon-star',
                'color' => 'orange'
            ),
            'personal' => array(
                'icon' => 'icon-user',
                'color' => 'brown'
            ),
            'account_settings' => array(
                'icon' => 'icon-cogs',
                'color' => 'lightgrey'
            ),
            'events' => array(
                'icon' => 'icon-calendar',
                'color' => 'blue'
            ),
            'document_center' => array(
                'icon' => 'icon-cloud-download',
                'color' => 'orange'
            ),
            'payroll' => array(
                'icon' => 'icon-credit-card', // icon-money
                'color' => 'green'
            ),
            ///////////////////////////////////
            'suggestions' => array(
                'icon' => 'icon-comment',
                'color' => 'green'
            ),
            'recruiting' => array(
                'icon' => 'icon-book',
                'color' => 'pink'
            ),
            'access_control' => array(
                'icon' => 'icon-lock',
                'color' => 'red'
            ),
            'employee_queries' => array(
                'icon' => 'icon-question-sign',
                'color' => 'satblue'
            ),
            'attendance' => array(
                'icon' => 'icon-time',
                'color' => 'satblue'
            ),
            'compensation' => array(
                'icon' => 'icon-money',
                'color' => 'green'
            ),
            'hr_clients' => array(
                'icon' => 'icon-group',
                'color' => 'magenta'
            ),
            'loans' => array(
                'icon' => 'icon-money',
                'color' => 'green'
            ),
            'employee_request' => array(
                'icon' => 'icon-inbox',
                'color' => 'satblue'
            ),
            'employee_purchases' => array(
                'icon' => 'icon-shopping-cart',
                'color' => 'green'
            ),
            'goals' => array(
                'icon' => 'glyphicon-soccer_ball',
                'color' => 'orange'
            ),
            'organizations' => array(
                'icon' => 'icon-sitemap',
                'color' => 'magenta'
            ),
            'billings' => array(
                'icon' => 'icon-money',
                'color' => 'lightgrey'
            ),
            'rewards' => array(
                'icon' => 'icon-money',
                'color' => 'orange'
            ),
            'training' => array(
                'icon' => 'icon-briefcase',
                'color' => 'orange'
            ),
            'auth' => array(
                'icon' => 'icon-key',
                'color' => 'orange'
            ),
			'analytics' => array(
				'icon' => 'glyphicon-stats',
				'color' => 'green'
			),
            'survey' => array(
                'icon' => 'icon-check',
                'color' => 'satblue'
            )
        );

        $combined_setup = array(
            'vacation',
            'compensation',
            'employee_request',
            'loans',
            'attendance',
            'payroll',
            'auth',
            'training',
            'appraisal',
            'goals',
        );

        $single_setup = array();

        $no_setup = array(
            'access_control',
            'account_settings',
            'employee_purchases',
            'hr_clients',
            'employee',
            'organisations',
            'billings',
            'rewards',
            'recruiting',
            'suggestions',
            'personal',
            'events',
            'document_center',
			'analytics'
        );
        
        $quick_access = ['document_center', 'rewards', 'goals', 'training', 'recruiting', 'events'];

        // Load CI object
        $CI = &get_instance();

        // Load libraries
        $CI->load->model('vacation/vacation_model', 'v_model');
        $CI->load->library('user_auth');


        $buffer = '<div class="row-fluid">
					<div class="span12">';

        if (is_array($nav_arr) and ! empty($nav_arr)) {

            $buffer .= '<ul class="tiles">';

            foreach ($nav_arr as $modules) {
                
                foreach ($modules as $link => $nav) {
                    if (in_array($nav['id_string'], $quick_access)){

                    //edited by obinna ukpabi
                    $isLoggedIn = $CI->session->userdata('id_user');
                    $id_company = $CI->session->userdata('id_company');
                    $employee = $CI->v_model->get('employees', array('id_user' => $isLoggedIn));

                    $setup = 0;

                    if (in_array($nav['id_string'], $no_setup) || $employee[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR || $employee[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
                        $setup = 1;
                    } elseif (in_array($nav['id_string'], $combined_setup) || $employee[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR || $employee[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
                        $md = $CI->v_model->get('modules', array('id_string' => $nav['id_string']));
                        $module_setup = $CI->v_model->get('module_setup', array('id_module' => $md[0]->id_module, 'status' => MODULE_SETUP_ACTIVE, 'id_company' => $id_company));

                        if (!empty($module_setup)) {
                            $setup = 1;
                        }
                    } else {
                        $setup = 0;
                    }

                    if ($setup > 0) {
                        $url = site_url($nav['id_string']);

                        if (isset($base_arr[$nav['id_string']])) {
                            $color = $base_arr[$nav['id_string']]['color'];
                            $icon = $base_arr[$nav['id_string']]['icon'];
                        } else {
                            $color = '';
                            $icon = '';
                        }

                        if ($nav['id_string'] == 'employee') {
                            //$url = site_url('employee/organogram');
                            $url = site_url('employee'); // Edited by Chuks
                        }


                        if ($nav['id_string'] == 'payroll') {
                            if ($employee[0]->id_access_level == ACCOUNT_TYPE_EMPLOYEE) {
                                if (!$CI->user_auth->have_perm(PAYROLL_RUN)) {
                                    $url = site_url('payroll/my_payslip');
                                }
                            } else if ($employee[0]->id_access_level == ACCOUNT_TYPE_UNIT_MANAGER) {
                                if (!$CI->user_auth->have_perm(PAYROLL_RUN)) {
                                    $url = site_url('payroll/my_payslip');
                                }
                            } else if ($employee[0]->id_access_level == ACCOUNT_TYPE_SECONDARY_ADMINISTRATOR) {
                                if (!$CI->user_auth->have_perm(PAYROLL_RUN)) {
                                    $url = site_url('payroll/my_payslip');
                                }
                            } else if ($employee[0]->id_access_level == ACCOUNT_TYPE_ADMINISTRATOR) {
                                if (!$CI->user_auth->have_perm(PAYROLL_RUN)) {
                                    $url = site_url('payroll/my_payslip');
                                }
                            }
                        }

                        $buffer .= '<li class="' . $color . ' dashboard_icon">
                                            <a href="' . $url . '">
                                                    <span>
                                                            <i class="' . $icon . '"></i>
                                                    </span>
                                                    <span class="name">
                                                            ' . $nav['subject'] . '
                                                    </span>
                                            </a>
                                    </li>';
                    }

                    unset($setup);
                    //end edit
                }
                }
            }

            $buffer .= '</ul>';
        }

        $buffer .= '</div>'
            . '</div>';

        return $buffer;
    }

}	